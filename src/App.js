import "./App.css";
import Register from "./component/auth/Register";
import Login from "./component/auth/Login";
import Header from "./pages/Header";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Dashboard from "./pages/Dashboard";

function App() {
  return (
      <Router>
        <Header />
        <Switch>
          <Route exact path="/register">
            <Register />
          </Route>
          <Route exact path="/">
            <Login />
          </Route>
          <Route exact path="/dashboard">
            <Dashboard />
          </Route>
        </Switch>
      </Router>
  );
}

export default App;
