import React, { Component } from 'react';
import { Link, Redirect,withRouter } from 'react-router-dom';
import { Button, Grid, TextField, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles'
import axios from 'axios';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      error: {
        email: '',
        password: ''
      },
    }

  }
  useStyles = makeStyles((theme) => ({
    grid: {
      "& .MuiGrid-spacing-xs-5": {
        width: 100

      }
    },
  }))

  componentDidMount() {
    console.log("mount");
    if (localStorage.email !== '') {
      console.log("hello" + localStorage);
      this.setState({
        email: localStorage.email,
        password: localStorage.password,
      })
    }
  }

  change = (e) => {

    const { name, value } = e.target;
    switch (name) {
      case 'password':
        // eslint-disable-next-line react/no-direct-mutation-state
        this.state.error.password = value.length < 3 ? 'Password at least 8 character' : '';
        break;
      default:
        break;
    }
    console.log(e.target.value)
    this.setState({ [e.target.name]: e.target.value });
  }
  login = (e) => {
    const {email,password} = this.state
    const loginUser = {
      email,
      password
    }
    e.preventDefault();
      var config = {
        method: 'post',
        url: 'http://localhost:4000/api/users/login',
        headers: {
          'Content-Type': 'application/json'
        },
        data: loginUser
      };
        axios(config)
        .then((response)=> {
          console.log(response.data);
          localStorage.setItem('token',response.data.token)
          if(response.data){
            this.props.history.push('/dashboard')
        }
        })
        .catch(function (error) {
          console.log(error);
        });
        console.log("HYGFF");
     }
  componentDidUpdate(){
    console.log("update");
  }
  render() {
    console.log("render11");
    const classes = this.useStyles
    if (localStorage.getItem('token') !== 'undefined' && localStorage.getItem('token')) {
      return <Redirect to="/dashboard" />
    }
    return (
      <>
        <Grid className={classes.grid} container direction="column" alignItems="center" justify="center" style={{ minHeight: "100vh" }} spacing={5}>
          <Grid container item lg={4} xs={12} sm={12} md={8} alignItems="center" justify="center" direction="column" spacing={5} >
            <Typography variant="h4" style={{ color: "#67b53f" }}>Form</Typography>
            <Grid item style={{ marginTop: "20px", justifyContent: "center", alignItems: "center", display: "flex", width: "80%" }}>
              <form>
                <TextField variant="outlined" label="Email" size="small" fullWidth style={{ marginBottom: "1px" }} name="email" value={this.state.email} onChange={this.change} />
                <TextField variant="outlined" label="Password" size="small" type="password" fullWidth style={{ marginBottom: "1px", borderRadius: '12px' }} name="password" value={this.state.password} onChange={this.change} />
                <Grid style={{ marginTop: "20px", display: "flex", justifyContent: "center", alignItems: "center" }}>
                  <Button size="large" variant="outlined" fullWidth onClick={this.login} style={{ color: "#67b53f", border: '1px solid', borderRadius: "4px" }}> Log In </Button>
                </Grid>
                <Grid style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
                <Link to="/register" style={{ color: "black" }}><b>Register New User</b></Link>
                </Grid>
              </form>
            </Grid>
          </Grid>
        </Grid>
      </>
    )
  }
}


export default withRouter(Login);