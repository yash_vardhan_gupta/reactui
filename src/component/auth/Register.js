import React, { Component } from 'react'
import {withRouter } from 'react-router-dom';
import { Button, Grid, TextField, Typography } from '@material-ui/core';
import axios from 'axios';

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            id:null,
            firstName: '',
            lastName: '',
            email: '',
            password: '',
            error: '',
            isUpdate:false
        }
    }
    change = (e) => {
        console.log(e.target.value)
        this.setState({ [e.target.name]: e.target.value });
    }
    submitUser = (e) => {
        e.preventDefault();
        const { name,id, email, password } = this.state;
            const newUser = {
                name,
                email,
                password
            }

            const updateUser = {
                name,
                email,
                id
            }
            //this.props.register(newUser);
            if(this.state.id!==null)
            this.updateUser(updateUser);
            else
            this.addUser(newUser);
            
    } 
    componentDidMount(){
        console.log("props",this.props.location.state)
        if(this.props.location.state){
            let {userUp} = this.props.location.state;
            console.log(userUp);
            this.setState({isUpdate:true})
            this.setState({name:userUp[0].name,email:userUp[0].email,id:userUp[0]._id})
       }
    }

     addUser = (data)=>{
         console.log("ADD")
        var config = {
            method: 'post',
            url: 'http://localhost:4000/api/users/register',
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        };

        axios(config)
            .then((response)=> {
                //console.log(JSON.stringify(response.data));
                console.log("Hello")
                if(response.data){
                    // console.log(response)
                    localStorage.setItem('token',response.data.token)
                    this.props.history.push('/dashboard')
                }
               

            })
            .catch(function (error) {
                console.log(error);
            });
    }

    updateUser = (data) =>{
        console.log("Update")
            let token = localStorage.getItem("token");
            console.log("Token",token)
            var config = {
            method: 'put',
            url: `http://localhost:4000/api/users/${data.id}?authorization=${token}`,
            headers: { 
                'Content-Type': 'application/json'
            },
            data : data
            };

            axios(config)
            .then((response)=> {
            console.log(JSON.stringify(response.data));
            this.props.history.push('/dashboard')
            })
            .catch(function (error) {
            console.log(error);
            });
    }
    render() {
        console.log("props",this.props.location.state)
       console.log(this.state)
        const buttonM = !this.state.isUpdate?'ADD':'UPDATE';
        return (
            <>
                <div className="d-flex justify-content-center text-align-center" >
                    <Typography variant="h5">{localStorage.getItem('token') !== 'undefined' ? 'User Loged in' : 'Need to Login or Signup'}</Typography>
                </div>
                <Grid container direction="column" alignItems="center" justify="center" style={{ minHeight: "100vh" }} spacing={5}>
                    <small id="passwordHelp" className="text-danger">
                        {this.state.error}
                    </small>
                    <Grid container item lg={4} xs={8} md={8} alignItems="center" justify="center" direction="column" spacing={5} style={{ border: "1px solid black", borderRadius: "20px" }}>
                        <Typography variant="h4" color="primary">Bloom</Typography>
                        <Typography variant="h5" color="primary">Sign Up</Typography>
                        <form>
                            <TextField variant="outlined" label="Name" fullWidth style={{ marginBottom: "1rem" }} name="name" value={this.state.name} onChange={this.change} />
                            {/* <TextField variant="outlined" label="Last Name" fullWidth style={{ marginBottom: "1rem" }} name="lastName" value={this.state.lastName} onChange={this.change} /> */}
                            <TextField variant="outlined" label="Email" fullWidth style={{ marginBottom: "1rem" }} name="email" value={this.state.email} onChange={this.change} />
                            {this.state.id==null?<TextField variant="outlined" label="Password" type="password" fullWidth style={{ marginBottom: "1rem" }} name="password" value={this.state.password} onChange={this.change} />:null}
                            <Button size="large" variant="contained" color="primary" onClick={this.submitUser} style={{ marginRight: '2px' }}> {buttonM} </Button>
                        </form>
                    </Grid>
                </Grid>
            </>
        )
    }
}


export default withRouter(Register);