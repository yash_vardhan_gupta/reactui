import React from "react";
// import { Table, Alert } from "react-bootstrap";
import { withRouter } from "react-router";
import axios from "axios";
// import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow'

class Users extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      users: [],
      response: {},
      message: null,
    };
  }
  getUsers() {
    var config = {
      method: "get",
      url: `http://localhost:4000/api/users`,
      headers: {
        authorization: localStorage.getItem("token"),
      },
    };
    axios(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
        this.setState({ users: response.data });
        console.log(this.users)
        this.setState({ message: null });
      })
      .catch(function (error) {
        console.log(error);
      });
  }
  componentDidMount() {
    this.getUsers();
  }
  componentDidUpdate(prevProps) {
    console.log("Updated Users prevProps", prevProps);
    console.log("Message ", this.state.message);
    if (this.state.message) {
      this.getUsers();
    }
  }

  deleteuser(userId) {
    var config = {
      method: "delete",
      url: `http://localhost:4000/api/users/${userId}`,
      headers: {
        authorization: localStorage.getItem("token"),
      },
    };

    axios(config)
      .then((response) => {
        console.log(response.data);
        this.setState({ message: response.data.message });
      })
      .catch(function (error) {
        console.log(error);
      });
  }
  edituser(id) {
    const filterData = this.state.users.filter((item) => item._id === id);
    this.props.history.push({
      pathname: "/register",
      state: { userUp: filterData },
    });
  }

  render() {
    const { error, users } = this.state;

    if (error) {
      return <div>Error: {error.message}</div>;
    } else {
      return (
        <TableContainer>
          <h2>user List</h2>
          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>ID</TableCell>
                <TableCell align="right">Name</TableCell>
                <TableCell align="right">Email</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {users.map((user) => (
                <TableRow key={user._id}>
                  <TableCell component="th" scope="row">
                    {user._id}
                  </TableCell>
                  <TableCell align="right">{user.name}</TableCell>
                  <TableCell align="right">{user.email}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      );
    }
  }
}

export default withRouter(Users);
