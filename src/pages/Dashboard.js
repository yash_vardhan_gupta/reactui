import React from "react";
import { Grid, Container } from "@material-ui/core";
import Users from "../component/auth/Users";

const Dashboard = () => {
  return (
    <React.Fragment>
      <Container
        maxWidth="lg"
        style={{
          marginTop: "10px",
          backgroundColor: "white",
          borderRadius: "15px",
        }}
      >
        <Grid
          alignItems="center"
          justify="center"
          style={{
            borderRadius: "10px",
            padding: "20px",
          }}
        >
          <p style={{ paddingLeft: "35px" }}>
            <b>User Details</b>
          </p>
        </Grid>
        <Users />
      </Container>
    </React.Fragment>
  );
};
export default Dashboard;
