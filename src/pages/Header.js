import React from 'react';
import '../index.css';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';


const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
  }));


class Header extends React.Component {
    logout=()=>{
        localStorage.removeItem('token');
        this.props.history.push('/')
    }
    register=()=>{
        this.props.history.push('/register')
    }
    usersList=()=>{
        this.props.history.push('/dashboard')
    }
    
    render(){
        const classes = useStyles;
        return (
            <div >
                <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6" className={classes.title}>
                        React UI
                    </Typography>
                    <Button color="inherit" onClick={this.register}>Register</Button>
                    <Button color="inherit" onClick={this.usersList}>Users</Button>
                    <Button color="inherit" onClick={this.logout}>Logout</Button>
                </Toolbar>
                </AppBar>
            </div>
        );
    }
   
}



export default withRouter(Header) ;